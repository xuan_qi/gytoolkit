# gytoolkit

#### 介绍
收录用于券商PB业务的管理工具

#### 软件架构
软件架构说明
1.  mailparser:操作gy mail
2.  ppwdbapi:smppw数据库api
3.  (开发中)datamanager:pb业务各项数据标准化处理

#### 安装教程

1.  pip install gytoolkit

#### 使用说明

1.  mailparser:参见[examples/mailparser.py](examples/mailparser.py)
2.  ppwdbapi:参见[examples/ppwdbapi.ipynb](examples/ppwdbapi.ipynb)
        


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
