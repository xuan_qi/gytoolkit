from gytoolkit.mailparser import MailClient
from gytoolkit import save_netvalue
import pandas as pd

def main(username,password):
    client = MailClient(username,password)
    
    print(f"目前邮件数为:{client.mail_num}\n")
    print("*"*60)
    #验证取邮件头
    headers  = client.get_mail_header(lookin_depth=10)
    print("目前邮箱中前10封邮件标题是:\n")
    print(pd.DataFrame(headers.values(),index=headers.keys()))
    print("*"*60)

    nv_list = client.parse_mails(headers)
    

    print("从前10封邮件解析出的净值情况:\n")
    print(pd.DataFrame(nv_list))
    save_netvalue(nv_list,"本地邮件净值解析.xlsx")
    print("净值已保存至本地")
    
if __name__=="__main__":
    username = input("请输入用户名:")
    password = input("请输入密码:")
    main(username,password)